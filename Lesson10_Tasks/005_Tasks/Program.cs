﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _005_Tasks
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Main Thread start.");

            var task = Task.Factory.StartNew(MyTask);

            //task.Start();

            Console.WriteLine("task1 Id: " + task.Id);

            // Метод Main() остается активным до завершения задачи MyTask(). 
            for (int i = 0; i < 10; i++)
            {
                Console.Write(".");
                Thread.Sleep(100);
            }

            task.Wait();

            Console.WriteLine();
            Console.WriteLine("Main thread has been completed. CurrentId = null? - {0}.", Task.CurrentId == null);

            Console.ReadLine();
        }

        static void MyTask()
        {
            Console.WriteLine("MyTask() # " + Task.CurrentId + " Start.");

            for (int count = 0; count < 10; count++)
            {
                Thread.Sleep(500);

                Console.WriteLine("MyTask() #" + Task.CurrentId + " count: " + count);
            }

            Console.WriteLine("MyTask() # " + Task.CurrentId + " Start.");
        }
    }
}