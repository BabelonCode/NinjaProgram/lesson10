﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _009_Tasks
{
    //https://docs.microsoft.com/en-us/dotnet/standard/threading/cancellation-in-managed-threads
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Starting Main Thread.");

            // Создать объект источника признаков отмены.
            var cancelTokSrc = new CancellationTokenSource();

            // Запустить задачу, передав признак отмены ей самой и делегату.
            Task task = Task.Factory.StartNew(MyTask, cancelTokSrc.Token, cancelTokSrc.Token);

            // Дать задаче возможность исполняться вплоть до ее отмены.
            Thread.Sleep(2000);

            try
            {
                // Отменить задачу.
                cancelTokSrc.Cancel();
                Console.WriteLine("Cancellation set in token source...");

                // Приостановить выполнение метода Main() до тех пор, 
                // пока не завершится задача - task.
                task.Wait();
            }
            catch (AggregateException e)
            {
                if (task.IsCanceled)
                    Console.WriteLine("\nThe wait operation was canceled.\n");

                Console.WriteLine("- " + e.InnerException.Message); // Для просмотра исключения снять комментарий.
            }
            finally
            {
                task.Dispose();
                cancelTokSrc.Dispose();
            }

            Console.WriteLine("Finish.");

            // Delay.
            Console.ReadKey();
        }


        static void MyTask(object ct)
        {
            var cancelTok = (CancellationToken)ct;

            // Прверить, отменена ли задача, прежде чем ее запускать.
            cancelTok.ThrowIfCancellationRequested();

            Console.WriteLine("Starting MyTask().");

            for (int count = 0; count < 10; count++)
            {
                // Для отслеживая отмены задачи применяется опрос.
                if (cancelTok.IsCancellationRequested)
                {
                    Console.WriteLine("Cancelling at task.");
                    cancelTok.ThrowIfCancellationRequested();
                }

                Thread.Sleep(500);
                Console.WriteLine("MyTask(), counter: " + count);
            }

            Console.WriteLine("MyTask() completed.");
        }
    }
}
