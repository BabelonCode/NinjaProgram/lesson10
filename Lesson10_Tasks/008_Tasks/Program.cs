﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _008_Tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            Task<int> task = Task.Factory.StartNew<int>(() => 
            {
                Console.WriteLine("Start Generic Task.");
                Thread.Sleep(2000);
                return 10;
            });

            Console.WriteLine("Result " + task.Result);
            Console.WriteLine("Finish");

            Console.ReadLine();
        }
    }
}
