﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _007_Tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            Task taskA = new Task(() => 
            {
                Console.WriteLine("Hello from taskA.");
                Console.ForegroundColor = ConsoleColor.Yellow;
                for (int count = 0; count < 50; count++)
                {
                    Thread.Sleep(100);
                    Console.Write("-");
                }
                Console.ResetColor();
                Console.WriteLine();
            });

            Task taskb = taskA.ContinueWith(t =>
            {
                Console.WriteLine("Hello from taskB.");
                Console.ForegroundColor = ConsoleColor.Green;
                for (int count = 0; count < 50; count++)
                {
                    Thread.Sleep(100);
                    Console.Write("^");
                }

                Console.ResetColor();
                Console.WriteLine();
            });

            taskA.Start();
            taskb.Wait();

            Console.WriteLine("Main thread has been completed.");

            Console.ReadLine();
        }

        static void MyTask(Task task)
        {

        }
    }
}
