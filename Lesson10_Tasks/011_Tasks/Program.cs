﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _011_Tasks
{
    class Program
    {
        static int[] data;

        // Метод служащий в качестве тела параллельно выполняемого цикла.
        // Операторы этого цикла просто расходуют время ЦП для целей демонстрации.
        static void MyTransform(int i)
        {
            data[i] = data[i] / 10;
            if (data[i] < 10000) data[i] = 0;
            if (data[i] > 10000 && data[i] < 20000) data[i] = 100;
            if (data[i] > 20000 && data[i] < 30000) data[i] = 200;
            if (data[i] > 30000) data[i] = 300;
        }

        static void MyTransform(int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = arr[i] / 10;
                if (arr[i] < 10000) arr[i] = 0;
                if (arr[i] > 10000 && arr[i] < 20000) arr[i] = 100;
                if (arr[i] > 20000 && arr[i] < 30000) arr[i] = 200;
                if (arr[i] > 30000) arr[i] = 300;
            }
        }

        static void Main()
        {
            Console.WriteLine("Starting Main Thread.");

            data = new int[100000000];

            // Инициализация данных в обычном цикле for.
            for (int i = 0; i < data.Length; i++)
                data[i] = i;

            DateTime dt = DateTime.Now;
            // Распараллелить цикл методом For().
            //Parallel.For(0, data.Length, new Action<int>(MyTransform));
            MyTransform(data);
            Console.WriteLine(DateTime.Now - dt);
            // Внимание!
            // Выполнение метода Main() приостанавливается, 
            // пока не произойдет возврат из метода For().

            Console.WriteLine("Main Thread completed.");

            Console.ReadLine();
        }
    }
}
